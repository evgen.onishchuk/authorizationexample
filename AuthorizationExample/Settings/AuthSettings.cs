﻿namespace AuthorizationExample.Settings;

public class AuthSettings
{
    public string AccessTokenSecret { get; set; }
    
    public string Issuer { get; set; }
    
    public string Audience { get; set; }
    
    public double AccessTokenExpirationMinutes { get; set; }
}