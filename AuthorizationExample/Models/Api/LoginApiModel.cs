﻿using System.ComponentModel.DataAnnotations;

namespace AuthorizationExample.Models.Api;

public class LoginApiModel
{
    [Required]
    public string Username { get; init; }
    
    [Required]
    public string Password { get; init; }

    public UserInfoModel ToAuthModel()
    {
        return new UserInfoModel
        {
            Password = Password,
            Username = Username
        };
    }
}