﻿namespace AuthorizationExample.Models;

public class AuthToken
{
    public required string AccessToken { get; init; }
    
    public required string RefreshToken { get; init; }
}