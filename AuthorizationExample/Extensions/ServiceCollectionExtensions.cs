﻿using System.Text;
using AuthorizationExample.Services;
using AuthorizationExample.Services.TokenGenerators;
using AuthorizationExample.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace AuthorizationExample.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services)
    {
        services.AddScoped<AuthService>();
        services.AddScoped<UserProvider>();
        services.AddSingleton<TokenGenerator>();
        services.AddSingleton<AccessTokenGenerator>();
        
        return services;
    }

    public static IServiceCollection AddAuthentication(this IServiceCollection services, IConfiguration configuration)
    {
        var authSettings = new AuthSettings();
        configuration.GetSection("auth").Bind(authSettings);
        services.AddSingleton(authSettings);
        
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ClockSkew = TimeSpan.FromMinutes(5),
                ValidateAudience = true,
                ValidAudience = authSettings.Audience,
                ValidateIssuer = true,
                ValidIssuer = authSettings.Issuer,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authSettings.AccessTokenSecret))
            };
        });
        
        return services;
    }
}