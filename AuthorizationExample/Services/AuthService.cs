﻿using AuthorizationExample.Models;
using AuthorizationExample.Services.TokenGenerators;

namespace AuthorizationExample.Services;

public class AuthService
{
    private readonly UserProvider _userProvider;
    private readonly AccessTokenGenerator _accessTokenGenerator;

    public AuthService(UserProvider userProvider, AccessTokenGenerator accessTokenGenerator)
    {
        _userProvider = userProvider;
        _accessTokenGenerator = accessTokenGenerator;
    }
    
    public async Task<AuthToken> AuthenticateUserAsync(UserInfoModel userInfoModel)
    {
        var user = await _userProvider.FindUserAsync(userInfoModel);
        if (user is null) return null;

        var accessToken = _accessTokenGenerator.GenerateToken(user);
        
        return new AuthToken
        {
            AccessToken = accessToken,
            RefreshToken = string.Empty
        };
    }
}