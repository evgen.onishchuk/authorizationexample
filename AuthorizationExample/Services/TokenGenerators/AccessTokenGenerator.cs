﻿using System.Security.Claims;
using AuthorizationExample.Models;
using AuthorizationExample.Settings;

namespace AuthorizationExample.Services.TokenGenerators;

public class AccessTokenGenerator
{
    private readonly TokenGenerator _tokenGenerator;
    private readonly AuthSettings _authSettings;

    public AccessTokenGenerator(TokenGenerator tokenGenerator, AuthSettings authSettings)
    {
        _tokenGenerator = tokenGenerator;
        _authSettings = authSettings;
    }
    
    public string GenerateToken(User user)
    {
        var claims = new List<Claim>()
        {
            new("id", user.Id.ToString()),
            new(ClaimTypes.Email, user.Email),
            new(ClaimTypes.Name, user.Name)
        };

        var expirationTime = DateTime.UtcNow.AddMinutes(_authSettings.AccessTokenExpirationMinutes);
        var token = _tokenGenerator.GenerateToken(
            _authSettings.AccessTokenSecret,
            _authSettings.Issuer,
            _authSettings.Audience,
            expirationTime,
            claims);

        return token;
    }
}