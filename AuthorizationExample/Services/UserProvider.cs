﻿using AuthorizationExample.Models;

namespace AuthorizationExample.Services;

public class UserProvider
{
    public Task<User> FindUserAsync(UserInfoModel userInfoModel)
    {
        //Find user by username in user store
        //Verify password
        //If user is not found or provide invalid password return null
        return Task.FromResult(new User
        {
            Id = Guid.NewGuid(),
            Email = "johndoe@mail.com",
            Name = "John Doe"
        });
    }
}