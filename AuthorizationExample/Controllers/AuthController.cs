﻿using AuthorizationExample.Models;
using AuthorizationExample.Models.Api;
using AuthorizationExample.Services;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationExample.Controllers;

[ApiController]
[Route("[controller]")]
public class AuthController: ControllerBase
{
    [HttpPost("login")]
    public async Task<ActionResult<AuthToken>> Login(LoginApiModel loginModel, [FromServices]AuthService authService)
    {
        var token = await authService.AuthenticateUserAsync(loginModel.ToAuthModel());
        if (token is null)
        {
            return Unauthorized();
        }

        return Ok(token);
    }
}